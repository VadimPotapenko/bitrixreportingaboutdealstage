<?php
require_once (__DIR__.'/core/Core.php');
define ('LOG_PATH', __DIR__.'/core/log/handler.txt');
define ('ENTITY', array(0 => 'AURUM', 4 => 'AURUM_INVEST', 9 => 'NICEDO', 11 => 'ARTJAR', 22 => 'HOME'));
#========================================== settings ========================================================#
$newApp = new Core(array('debugErrors' => 1, 'debugPath' => LOG_PATH, 'debugBool' => 1));
if (isset($_REQUEST['data']['FIELDS']['ID'])) {
	$deal = $newApp->callCRest('call', 'crm.deal.list', array('filter' => array('ID' => $_REQUEST['data']['FIELDS']['ID']), 'select' => array('ID', 'STAGE_ID', 'CATEGORY_ID', 'UF_CRM_1558447938')));
	$key = !stripos($deal['result'][0]['STAGE_ID'], ':') ? 'C0_'.$deal['result'][0]['STAGE_ID'] : preg_replace('~:~', '_', $deal['result'][0]['STAGE_ID']);
	$newApp->writeToLog($deal, $_REQUEST['event']);
}

if (isset($_REQUEST['event']) && $_REQUEST['event'] == 'ONCRMDEALADD') {
	### новый элемент ###
	$newItem = $newApp->callCRest('call', 'entity.item.add', array(
		'ENTITY'          => ENTITY[$deal['result'][0]['CATEGORY_ID']],
		'NAME'            => $deal['result'][0]['ID'],
		'PROPERTY_VALUES' => array($key => strtotime('today'), 'CURRENT' => $deal['result'][0]['STAGE_ID'])
	));
	$newApp->writeToLog($newItem, 'ENTITY-ADD');

} elseif (isset($_REQUEST['event']) && $_REQUEST['event'] == 'ONCRMDEALUPDATE') {
	### обновление элемента ###
	$itemGet = $newApp->callCrest('call', 'entity.item.get', array(
		'ENTITY' => ENTITY[$deal['result'][0]['CATEGORY_ID']],
		'filter' => array('NAME' => $deal['result'][0]['ID'])
	));
	$newApp->writeToLog($itemGet, 'Проврка существования элемента хранилища');
	if (!empty($itemGet['result'])) {
		### существует ###
		$stageList = $newApp->callCrest('call', 'crm.dealcategory.stage.list', array('id' => $deal['result'][0]['CATEGORY_ID']));
		$res = $newApp->callCrest('call', 'crm.dealcategory.stage.list', array('ID' => $deal['result'][0]['CATEGORY_ID']));
		foreach ($res['result'] as $value) {
			$stages[] = !stristr($value['STATUS_ID'], ':') ? 'C0_'.$value['STATUS_ID'] : preg_replace('~:~', '_', $value['STATUS_ID']);
		}
		for ($i = 0, $s = count($stages); $i < $s; $i++) {
			### получаем стадии до текущей ###
			if (preg_replace('~:~', '_', $stages[$i]) == $key) {
				$stagesBefore[] = $stages[$i];
				$newApp->writeToLog(preg_replace('~:~', '_', $stages[$i]).' == '.$key, 'Последняя стадия до');
				break;
			}
			$stagesBefore[] = $stages[$i];
		}

		$bool = false;
		foreach ($itemGet['result'][0]['PROPERTY_VALUES'] as $k => $v) {
			### проверка есть ли данные в стадиях после текущей ###
			if (!in_array($k, $stagesBefore)) {
				if($k == 'CURRENT') continue;
				if (!empty($v)) {
					$newApp->writeToLog($k.'='.$v, 'Стадия с данными до');
					$bool = true;
				}
			}
		}

		$newApp->writeToLog($itemGet['result'][0]['PROPERTY_VALUES']['CURRENT'].' == '.$deal['result'][0]['STAGE_ID'], 'Проверка изменения стадии сделки');
		if ($itemGet['result'][0]['PROPERTY_VALUES']['CURRENT'] != $deal['result'][0]['STAGE_ID']) {
			if ($bool) {
				### вычисление значения предыдущей стадии ###
				foreach ($stagesBefore as $value) {
					if (!empty($itemGet['result'][0]['PROPERTY_VALUES'][$value])) $tmpStagesBefore = $value;
					if ($itemGet['result'][0]['PROPERTY_VALUES']['CURRENT'] == $deal['result'][0]['STAGE_ID']) break;
				}
				$lastSatage = $tmpStagesBefore;
				$valueLastSatage = strtotime('today') - $itemGet['result'][0]['PROPERTY_VALUES'][$lastSatage];
				$newApp->writeToLog($lastSatage.' = '.$valueLastSatage, 'Последняя стадия после и ее данные');
				### если да обнуляем их и перезаписываем текщую ###
				foreach ($itemGet['result'][0]['PROPERTY_VALUES'] as $k => $v) {
					if (in_array($k, $stagesBefore)) continue;
					$propertyData[] = array(
						'method' => 'entity.item.update',
						'params' => array(
							'ENTITY'          => ENTITY[$deal['result'][0]['CATEGORY_ID']],
							'ID'              => $itemGet['result'][0]['ID'],
							'PROPERTY_VALUES' => array($k => '')
						)
					);
				}
				$propertyData[] = array(
					'method' => 'entity.item.update',
					'params' => array(
						'ENTITY'          => ENTITY[$deal['result'][0]['CATEGORY_ID']],
						'ID'              => $itemGet['result'][0]['ID'],
						'PROPERTY_VALUES' => array($key => strtotime('today'), 'CURRENT' => $deal['result'][0]['STAGE_ID'], $lastSatage => $valueLastSatage)
					)
				);
				if (count($propertyData) > 50) $propertyData = array_chunk($propertyData, 50);
				else $propertyData = array($propertyData);
				for ($i = 0, $s = count($propertyData); $i < $s; $i++) {
					$propertyUpdate = $app->callCrest('callBatch', $propertyData[$i]);
				}
				$newApp->writeToLog($propertyUpdate, 'Обновление с обнулением');

			} else {
				### вычисление значения предыдущей стадии ###
				$lastSatage = !stristr($itemGet['result'][0]['PROPERTY_VALUES']['CURRENT'], ':') ?
					'C0_'.$itemGet['result'][0]['PROPERTY_VALUES']['CURRENT'] :preg_replace('~:~', '_', $itemGet['result'][0]['PROPERTY_VALUES']['CURRENT']);
				$valueLastSatage = strtotime('today') - $itemGet['result'][0]['PROPERTY_VALUES'][$lastSatage];
				$newApp->writeToLog($lastSatage.' => '.$valueLastSatage, 'Последняя стадия при движении сделки вперед');
				### апдейт ###
				$propertyUpdate = $newApp->callCrest('call', 'entity.item.update', array(
					'ENTITY'          => ENTITY[$deal['result'][0]['CATEGORY_ID']],
					'ID'              => $itemGet['result'][0]['ID'],
					'PROPERTY_VALUES' => array($key => strtotime('today'), 'CURRENT' => $deal['result'][0]['STAGE_ID'], $lastSatage => $valueLastSatage)
				));
				$newApp->writeToLog($propertyUpdate, 'Просто обновление');
			}
		}

	} else {
		### отсутствует ###
		$newItem = $newApp->callCrest('call', 'entity.item.add', array(
			'ENTITY'          => ENTITY[$deal['result'][0]['CATEGORY_ID']],
			'NAME'            => $deal['result'][0]['ID'],
			'PROPERTY_VALUES' => array($key => strtotime('today'), 'CURRENT' => $deal['result'][0]['STAGE_ID'])
		));
		$newApp->writeToLog($newItem, 'Создали уже существующий элемент');
	}

} elseif (isset($_REQUEST['event']) && $_REQUEST['event'] == 'ONCRMDEALDELETE') {
	### удаление элемента ###
	$itemGet = $newApp->callCRest('call', 'entity.item.get', array(
		'ENTITY' => ENTITY[$deal['result'][0]['CATEGORY_ID']],
		'filter' => array('NAME' => $deal['result']['ID'])
	));
	$itemDelete = $newApp->callCRest('call', 'entity.item.delete', array('ENTITY' => ENTITY, 'ID' => $itemGet['result']['ID']));
}