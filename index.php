<?php
require_once (__DIR__.'/core/Core.php');
define ('LOG_PATH', __DIR__.'/core/log/index.txt');
define ('USER_FIELD', '2648');
define ('USER_FIELD_NAME', 'UF_CRM_1558447938');
define ('ENTITY', array(0 => 'AURUM', 4 => 'AURUM_INVEST', 9 => 'NICEDO', 11 => 'ARTJAR', 22 => 'HOME'));
$mainArr = array();
$arr_stages = false;
$currentClient = $_POST;
#============================================ settings =================================================#
$newApp = new Core(array('debugBool' => 1, 'debugPath' => LOG_PATH));
$current_user = $newApp->noDataCrest('callCurrentUser');
### КОНФИГУРАЦИИ ФИЛЬТРА: пользователи портала ###
$users = $newApp->callCrest('callBatchList', 'user.get', array('filter' => array('ACTIVE' => '1')));
foreach ($users as $value) {
	foreach ($value['result']['result'] as $v) {
		foreach ($v as $tmp) {
			if (empty($tmp['NAME'])) continue;
			$arr_users[$tmp['ID']] = $tmp['LAST_NAME'].' '.$tmp['NAME'];
		}
	}
}
### направления ###
$arr_categories[0] = 'АУРУМ';
$categories = $newApp->callCrest('callBatchList', 'crm.dealcategory.list', array());
foreach ($categories as $value) {
	foreach ($value['result']['result'] as $v) {
		foreach ($v as $tmp) {
			$arr_categories[$tmp['ID']] = $tmp['NAME'];
		}
	}
}

### группы ###
$groups = $newApp->callCrest('call', 'crm.deal.userfield.list', array('filter' => array('ID' => USER_FIELD), 'select' => array('LIST')));
foreach ($groups['result'][0]['LIST'] as $group) { $arr_groups[$group['ID']] = $group['VALUE']; }
### настройка фильтра и запуск вычисления данных ###
if (isset($_POST['category'])) {
	$tmpDate = strtotime($_POST['date-to'] ?: date('Y-m-d')) + 86400;
	$lastDate = date('Y-m-d', $tmpDate);
	$deal_filter =  array(
		'>=DATE_CREATE'  => $_POST['date-from'] ?: date('Y-m-d'),
		'<=DATE_CREATE'  => $lastDate,
		'ASSIGNED_BY_ID' => ($_POST['assigned'] != 'empty') ? $_POST['assigned'] : '',
		'CATEGORY_ID'    => $_POST['category'],
		USER_FIELD_NAME  => ($_POST['group'] != 'empty') ? $_POST['group'] : ''
	);
	foreach ($deal_filter as $k => $v) {
		if (empty($v) && $k != 'CATEGORY_ID') unset($deal_filter[$k]);
	}
	$stages_filter = array('ID' => $_POST['category']);
	### стадии ###
	$stages = $newApp->callCrest('call', 'crm.dealcategory.stage.list', $stages_filter);
	foreach ($stages['result'] as $value) {
		$arr_stages[preg_replace('~_~', ':', $value['STATUS_ID'])] = $value['NAME'];
		$stageEl[] = stripos($value['STATUS_ID'], ':') ? $value['STATUS_ID'] : 'C0:'.$value['STATUS_ID'];
	}
	### КОНФИГУРАЦИИ ХРАНИЛИЩА: хранилище ?: ###
	$entity = $newApp->callCrest('call', 'entity.get', array('ENTITY' => ENTITY[$deal_filter['CATEGORY_ID']]));
	if (!isset($entity['result']) && isset($entity['error'])) {
		$entity = $newApp->callCrest('call', 'entity.add', array('ENTITY' => ENTITY[$deal_filter['CATEGORY_ID']], 'NAME' => ENTITY[$deal_filter['CATEGORY_ID']]));;
		if (empty($entity['result']) || !empty($entity['error'])) $newApp->showSystemMessage('noentity.php', 1);
	}
	### свойства элементов ?: ###
	$property = $newApp->callCrest('call', 'entity.item.property.get', array('ENTITY' => ENTITY[$deal_filter['CATEGORY_ID']]));
	if (empty($property['result'])) {
		### ранее не были созданы ###
		foreach ($stages['result'] as $value) {
			if (!stripos($value['STATUS_ID'], ':')) $key = 'C0:'.$value['STATUS_ID'];
			else $key = $value['STATUS_ID'];
			$propetyData[] = array(
				'method' => 'entity.item.property.add',
				'params' => array(
					'ENTITY'   => ENTITY[$deal_filter['CATEGORY_ID']],
					'PROPERTY' => preg_replace('~:~', '_', $key),
					'NAME'     => $key
				)
			);
		}
		if (count($propetyData) > 50) $propetyData = array_chunk($propetyData, 50);
		else $propetyData = array($propetyData);
		for ($i = 0, $s = count($propetyData); $i < $s; $i++) {
			$propertyAdd = $newApp->callCrest('callBatch', $propetyData[$i]);
		}
		if (empty($propertyAdd[0]['result']['result']) || !empty($propertyAdd[0]['result']['result_error'])) $newApp->showSystemMessage('noentity.php', 1);

	} else {
		### проверяем вхождение стадий и свойств элементов ###
		foreach ($property['result'] as $value) { $propEl[] = $value['NAME']; }
		if ($propDiff = array_diff($stageEl, $propEl)) {
			foreach ($propDiff as $value) {
				$updateProp = $newApp->callCrest('call', 'entity.item.property.add', array(
					'ENTITY'   => ENTITY[$deal_filter['CATEGORY_ID']],
					'PROPERTY' => preg_replace('~:~', '_', $value),
					'NAME'     => $value
				));
			}
		}
	}

	### ОСНОВНОЙ МАССИВ ###
	$deals = $newApp->callCrest('callBatchList', 'crm.deal.list', array('filter' => $deal_filter, 'select' => array('ID', 'STAGE_ID', 'TITLE', 'ASSIGNED_BY_ID')));
	if (isset($deals[0]['result']['result'][0]) && !empty($deals[0]['result']['result'][0])) {
		### заготовка с данными из сделки ###
		foreach ($deals as $deal) {
			foreach ($deal['result']['result'] as $value) {
				foreach ($value as $v) {
					$dealCamp[$v['ID']] = array('title' => $v['TITLE'], 'stage' => $v['STAGE_ID'], 'id' => $v['ID']);//, 'responsible' => $v['ASSIGNED_BY_ID']); wtf?
					foreach ($users as $user) {
						foreach ($user['result']['result'] as $items) {
							foreach ($items as $item) {
								if ($v['ASSIGNED_BY_ID'] == $item['ID']) $dealCamp[$v['ID']]['responsible'] = $item['LAST_NAME'].' '.$item['NAME'];
							}
						}
					}
				}
			}
		}

		$flag = array();
		$elements = $newApp->callCrest('call', 'entity.item.get', array('ENTITY' => ENTITY[$deal_filter['CATEGORY_ID']],'filter' => array('NAME' => array_keys($dealCamp))));
		if (isset($elements['result']) && !empty($elements['result'])) {
			### вычисляем индекс сортировки ###
			foreach ($elements['result'] as $v) {
				foreach ($v['PROPERTY_VALUES'] as $k => $value) {
					$tmp = !stristr($dealCamp[$v['NAME']]['stage'], ':') ? 'C0:'.$dealCamp[$v['NAME']]['stage'] : preg_replace('~_~', ':', $dealCamp[$v['NAME']]['stage']);
					if (preg_replace('~_~', ':', $k) == $tmp) {
						$flag[$v['NAME']] += 1;
						break;
					} else { $flag[$v['NAME']] += 1; }
				}
			}
			### формируем основной массив ###
			foreach ($elements['result'] as $v) {
				$mainArr[$v['NAME']] = array(
					$flag[$v['NAME']] => array(
						'id'          => $dealCamp[$v['NAME']]['id'],
						'title'       => $dealCamp[$v['NAME']]['title'],
						'responsible' => $dealCamp[$v['NAME']]['responsible']
					)
				);
				foreach ($v['PROPERTY_VALUES'] as $k => $st) {
					if ($k == 'CURRENT') continue;
					if (stristr($k, 'C0')) $k = substr($k, 3);
					$tmp = preg_replace('~_~', ':', $k);
					$currentStageDeal = $dealCamp[$v['NAME']]['stage'];
					if ($tmp == $currentStageDeal) {
						$date = strtotime('today') - $st;
						$mainArr[$v['NAME']][$flag[$v['NAME']]][$tmp] = $date / 86400;
					} else {
						if (!empty($st) || $st == '0') $mainArr[$v['NAME']][$flag[$v['NAME']]][$tmp] = $st / 86400;
						else $mainArr[$v['NAME']][$flag[$v['NAME']]][$tmp] = '-';
					}
				}
			}
			### сортировка лесенка ###
			usort($mainArr, function($a, $b){ if (key($a) < key($b)) return 1; });
			### формирум массив для excel ###
			foreach ($mainArr as $k => $value) {
				foreach ($value as $v) {
					$excelArr[$k] = array('Сделка' => $v['title'], 'Ответственный' => $v['responsible']);
					foreach ($v as $lastKey => $lastValue) {
						if (in_array($lastKey, array('id', 'title', 'responsible'))) continue;
						$excelArr[$k][$arr_stages[$lastKey]] = !stristr($lastValue, '-') ? $lastValue : 'empty';
					}
				}
			}
			$newApp->saveParams($excelArr, __DIR__.'/core/config/handlers/excel_data/'.$current_user['user']['ID'].'_excel.php');
		}
	}
}
### рендерим результат ###
$newApp->render(array(
	'currentUser'   => $current_user,
	'currentClient' => $currentClient,
	'users'         => $arr_users,
	'category'      => $arr_categories,
	'groups'        => $arr_groups,
	'table'         => array('stages' => $arr_stages, 'main' => $mainArr ?: '')
), 'table');