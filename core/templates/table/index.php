		<?php include(__DIR__.'/layouts/header.php'); ?>
			<!-- Start Form -->
			<header class='container-fluid bg-primary'>
				<form method='post' class='text-white'>
					<p class='text-white h3 p-3 text-center'>Отчет по стадиям сделок</p>
					<div class='row'>
						<!-- ! -->
						<?php if (isset($data['currentClient']) && !empty($data['currentClient'])): ?>
							<input name='AUTH_ID' type="text" value="<?=$data['currentClient']['AUTH_ID'];?>" class='disable'>
							<input name='AUTH_EXPIRES' type="text" value="<?=$data['currentClient']['AUTH_EXPIRES'];?>" class='disable'>
							<input name='REFRESH_ID' type="text" value="<?=$data['currentClient']['REFRESH_ID'];?>" class='disable'>
							<input name='member_id' type="text" value="<?=$data['currentClient']['member_id'];?>" class='disable'>
							<input name='status' type="text" value="<?=$data['currentClient']['status'];?>" class='disable'>
							<input name='PLACEMENT' type="text" value="<?=$data['currentClient']['PLACEMENT'];?>" class='disable'>
						<?php endif; ?>

						<!-- Ответственный -->
						<div class="form-group col">
							<label for="formGroupExampleInput">Ответственный:</label>
							<select name='assigned' class="custom-select mr-sm-2" id="inlineFormCustomSelect">
								<option value='empty'>Не установлено</option>
								<?php foreach($data['users'] as $key => $user): ?>
									<?php $selected = (isset($_POST['assigned']) && $_POST['assigned'] == $key) ? 'selected' : '' ?>
					    			<option <?=$selected;?> value="<?=$key;?>"><?=$user;?></option>
					    		<?php endforeach; ?>
					    	</select>
						</div>

						<!-- Временной период -->
						<div class="form-group col">
							<label for="formGroupExampleInput2">Начало временного периода:</label>
							<input type="date" name='date-from' class="form-control" id="formGroupExampleInput2" placeholder="Пример пункта формы" value="<?=$_POST['date-from'];?>">
						</div>

						<div class='form-group col'>
							<label class="mr-sm-2" for="inlineFormCustomSelect">Конец временного периода:</label>
					    	<input type="date" name='date-to' class="form-control" id="formGroupExampleInput2" placeholder="Пример пункта формы" value="<?=$_POST['date-to'];?>">
					    </div>

					    <!-- Направление -->
					    <div class='form-group col'>
							<label class="mr-sm-2" for="inlineFormCustomSelect">Направление:</label>
					    	<select name='category' class="custom-select mr-sm-2" id="inlineFormCustomSelect">
					    		<?php foreach($data['category'] as $key => $category): ?>
					    			<?php $selectedCat = (isset($_POST['category']) && $_POST['category'] == $key) ? 'selected' : '' ?>
						    		<option <?=$selectedCat;?> value="<?=$key;?>"><?=$category;?></option>
					    		<?php endforeach; ?>
					    	</select>
					    </div>

					    <!-- Группа -->
					    <div class='form-group col'>
							<label class="mr-sm-2" for="inlineFormCustomSelect">Группа:</label>
					    	<select name='group' class="custom-select mr-sm-2" id="inlineFormCustomSelect">
					    		<option value='empty'>Не установлено</option>
					    		<?php foreach($data['groups'] as $key => $group): ?>
					    			<?php $selectedGrp = (isset($_POST['group']) && $_POST['group'] == $key) ? 'selected' : '' ?>
						    		<option <?=$selectedGrp;?> value="<?=$key;?>"><?=$group;?></option>
					    		<?php endforeach; ?>
					    	</select>
					    </div>
					</div>

					<div class='row'>
						<div class='col'>
							<input type="submit" name="name" class='btn btn-success m-2 ml-5' value='Сформировать'>
						</div>
						<div class='col-2'>
							<a href="core/config/handlers/excel.php?id=<?=$data['currentUser']['user']['ID'];?>" class='btn btn-success m-2 pl-5 pr-5'>Excel</a>
						</div>
					</div>
				</form>
			</header>
			<!-- End Form -->

			<!-- Start Table -->
			<main class='container-fluid bg-light main-wrap'>
				<?php if (isset($data['table']['stages']) && !empty($data['table']['stages'])): ?>
					<?php if (!empty($data['table']['main'])): ?>
						<table class='table table-sm text-center table-hover container'>
							<thead class='text-white'>
								<tr>
									<th class='bg-info'>Сделка</th>
									<th class='bg-info'>Ответственный</th>
									<?php foreach($data['table']['stages'] as $stage): ?>
										<th class='bg-info'><?=$stage;?></th>
									<?php endforeach; ?>
								</tr>
							</thead>

							<tbody>
								<?php foreach ($data['table']['main'] as $id => $items): ?>
									<tr>
										<?php foreach ($items as $item): ?>
											<td><a href="https://shurup.bitrix24.ru/crm/deal/details/<?=$item['id'];?>/" target="_blank" style='font-size:16px'><?=$item['title'];?></a></td>
											<td><?=$item['responsible'];?></td>
											<?php foreach ($data['table']['stages'] as $k => $v): ?>
												<td><?=$item[preg_replace('~_~', ':', $k)];?></td>
											<?php endforeach; ?>
										<?php endforeach; ?>
									</tr>
								<?php endforeach; ?>
							</tbody>

							<tfoot class='text-white'></tfoot>
						</table>
					<?php else: ?>
						<p class='text-danger h4 text-center p-4'>Данные не найдены</p>
					<?php endif; ?>
				<?php else: ?>
					<p class='text-danger h4 text-center p-4'>Необходимо выбрать 'направление' для расчета данных</p>
				<?php endif; ?>
			</main>
			<!-- End Table -->
				
			<section class='container-fluid bg-primary'>
				<p class='text-white text-right py-3'></p>
			</section>
		<?php include(__DIR__.'/layouts/footer.php'); ?>