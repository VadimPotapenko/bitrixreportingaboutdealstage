<?php
require_once 'excel_data/'.$_GET['id'].'_excel.php';
function array2csv($array){
	if (count($array) == 0) return null;
	ob_start();
	$df = fopen("php://output", 'w');
	fputs($df, chr(0xEF) . chr(0xBB) . chr(0xBF));
	foreach ($array as $row) { fputcsv($df, $row, ';', '"'); }
	fclose($df);
	return ob_get_clean();
}
function download_send_headers($filename) {
	$now = gmdate("D, d M Y H:i:s");
	header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
	header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
	header("Last-Modified: {$now} GMT");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment;filename={$filename}");
	header("Content-Transfer-Encoding: binary");
}
function formatArray ($arr) {
	$data[] = array_keys(current($arr));
	for ($i = 0, $s = count($arr); $i < $s; $i++) { $data[] = array_values($arr[$i]); }
	return $data;
}
download_send_headers("data_export_" . date("Y-m-d") . ".csv");
echo array2csv(formatArray($appsConfig));
die();