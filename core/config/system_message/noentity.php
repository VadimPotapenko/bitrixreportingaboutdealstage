<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>System Message</title>
		<link rel="stylesheet" type="text/css" href="core/templates/default/bootstrap/css/bootstrap.css">
	</head>
	<body>
		<p class='text-white h1 bg-danger'>Обнаружена ошибка в работе приложения с хранилищем. Обратитесь к разработчику.</p>
	</body>
</html>